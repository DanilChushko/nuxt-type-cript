export default $axios => resource => ({
  index (payload = {}) {
    return $axios.$get(`${resource}`, { params: payload })
  },

  show (id, payload = {}) {
    return $axios.$get(`${resource}/${id}`, { params: payload })
  },

  create (payload) {
    return $axios.$post(`${resource}`, payload)
  },

  update (id, payload) {
    return $axios.$put(`${resource}/${id}`, payload)
  },

  delete (id) {
    return $axios.$delete(`${resource}/${id}`)
  }
})

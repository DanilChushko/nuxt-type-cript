module.exports = {
  API_URL: 'https://jsonplaceholder.typicode.com',
  HOST: '0.0.0.0',
  PORT: '3000'
}

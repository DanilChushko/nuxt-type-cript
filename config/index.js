const devConfig = require('./development')

const env = process.env.NODE_ENV || 'development'

const envSpecificConfigs = {
  development: devConfig
}

const specific = envSpecificConfigs[env]

if (!specific) {
  throw new TypeError(
    env + ' is not a valid NODE_ENV. Please, use production or development'
  )
}

module.exports = { ...specific }

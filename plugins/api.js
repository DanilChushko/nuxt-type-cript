import createApiInterface from '~/api'

export default (ctx, inject) => {
  const createApiInterfaceWithAxios = createApiInterface(ctx.$axios)

  inject('posts', createApiInterfaceWithAxios('/posts'))
}
